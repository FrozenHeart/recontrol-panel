[![@coreui react](https://img.shields.io/badge/@coreui%20-react-lightgrey.svg?style=flat-square)](https://github.com/coreui/react)
[![npm package][npm-coreui-react-badge]][npm-coreui-react]

[npm-coreui-react]: https://www.npmjs.com/package/@coreui/react
[npm-coreui-react-badge]: https://img.shields.io/npm/v/@coreui/react.png?style=flat-square


# reControl Panel

Panel de gestión de alquileres vacacionales, desarrollado por Oscar Fernández

## Instalación

``` bash
# clone el repositorio
$ git clone https://gitlab.com/FrozenHeart/recontrol-panel.git my-project

# navege hasta la carpeta del proyecto
$ cd my-project

# instale las dependencias
$ npm install
```


### Uso Básico

``` bash
# Servidor con recarga automática http://localhost:3000
$ npm start
```

Ingresar a [http://localhost:3000](http://localhost:3000). La web recarga automáticamente cuando se realizan cambios en el código.

### Build

Ejecute el comando `build` para compilar el proyecto, cuando finalize se crearán todos los archivos en `build/` listo para produccion..

```bash
# compilar para producción minificada
$ npm run build
```

