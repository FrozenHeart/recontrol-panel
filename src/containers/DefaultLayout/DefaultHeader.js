import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'
import { withFirebase } from '../../components/Firebase';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      email: null,
      uid: '1',
      loggedUser: []
    };
  }
  componentDidMount () {
    //When auth state is changed
    this.listener = this.props.firebase.auth.onAuthStateChanged(
      authUser => {
        authUser
          ? this.setState({ email: authUser.email, uid: authUser.uid })
          : this.props.history.push('/login');
      },
    );
    //Load user data with email
    this.props.firebase.db.ref('users').child(this.state.uid).on('value', snapshot => {
      if(snapshot.exists())
      {
          const values = snapshot.val(); 
          this.setState({
              loggedUser: values
          });
      }
    });
  }
  componentWillUnmount() {
    this.listener();
  }
  render() {

    // eslint-disable-next-line
    const { firebase, children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Resumen</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Planner</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link">Channel Manager</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={this.state.loggedUser.avatar} className="img-avatar" alt={this.state.email} />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center"><strong>{this.state.loggedUser.name}</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Cuenta</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Ajustes</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Pagos</DropdownItem>
              <DropdownItem divider />
              <DropdownItem onClick={firebase.doSignOut}><i className="fa fa-lock"></i> Salir</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withFirebase(DefaultHeader);
