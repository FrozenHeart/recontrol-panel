import React from 'react';
import { withFirebase } from '../../../components/Firebase';
import {

    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Input,
    Label,
    Form,
    FormGroup,

  } from 'reactstrap';

class CreateProperty extends React.Component {

    render () {
        return (
        <Row>
            <Col xs="12" sm="12">
                <Card>
                    <CardHeader>
                        <strong>Nuevo Alojamiento</strong>
                        <small> El simbolo (*) indica un campo requerido</small>
                    </CardHeader>
                    <CardBody>
                        <Form>
                        <strong>Datos generales</strong>
                            <FormGroup>
                            <Label htmlFor="name">Nombre o Referencia (*)</Label>
                            <Input type="text" id="name" name="name" placeholder="Ingrese un nombre o una referencia para su alojamiento" required />
                            </FormGroup>
                            <FormGroup>
                            <Label htmlFor="vat">VAT</Label>
                            <Input type="text" id="vat" placeholder="DE1234567890" />
                            </FormGroup>
                            <FormGroup>
                            <Label htmlFor="street">Street</Label>
                            <Input type="text" id="street" placeholder="Enter street name" />
                            </FormGroup>
                            <FormGroup row className="my-0">
                            <Col xs="8">
                                <FormGroup>
                                <Label htmlFor="city">City</Label>
                                <Input type="text" id="city" placeholder="Enter your city" />
                                </FormGroup>
                            </Col>
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="postal-code">Postal Code</Label>
                                <Input type="text" id="postal-code" placeholder="Postal Code" />
                                </FormGroup>
                            </Col>
                            </FormGroup>
                            <FormGroup>
                            <Label htmlFor="country">Country</Label>
                            <Input type="text" id="country" placeholder="Country name" />
                            </FormGroup>
                        </Form>
                        
                    </CardBody>
                </Card>
            </Col>
        </Row>
        )
    }
}
export default withFirebase(CreateProperty)