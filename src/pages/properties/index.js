import React from 'react';
import { withFirebase } from '../../components/Firebase';
import PropItem from '../../components/Properties/PropItem';
import { ContextMenu, MenuItem } from "react-contextmenu";
import PropTypes from 'prop-types';
 
import {
    Pagination,
    PaginationItem,
    PaginationLink,
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
    Button,
  } from 'reactstrap';

    class PropertiesIndex extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                properties: []
            }
        }
        componentDidMount() {
            this.loadProperties();
        }
        loadProperties = () => {
            this.props.firebase.db.ref().child('properties').orderByChild('createdAt').on('value', snapshot => {
                const PropertiesList = snapshot.val();
                this.setState({
                    properties: PropertiesList
                })
            });
        }
        static contextTypes = {
            router: PropTypes.object
        }
        redirectToTarget = () => {
            this.context.router.history.push('/properties/create')
        }

        handleClick(e, data) {
            alert(data.foo.name);
        }
        loading = () => <div className="animated fadeIn pt-1 text-center">Cargando...</div>
        render() {
            const { properties } = this.state;

            return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="12" sm="12" lg="12">
                    <Card>
                    <CardHeader>
                        Propiedades
                    </CardHeader>
                    <CardBody>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col xs="9" sm="9" lg="9" style={{ textAlign: "center" }}>
                                        <p>A continuación se encuentra la tabla con todas las propiedades que se registraron, puede presionar el click derecho sobre una propiedad para realizar acciones con ella.</p>
                                    </Col>
                                    <Col xs="3" sm="3" lg="3">
                                    <Button block color="primary" onClick={this.redirectToTarget}>Registrar Propiedad</Button>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                        <Table responsive>
                        <thead>
                        <tr>
                            <th>Propiedad</th>
                            <th>Propietario</th>
                            <th>Algo</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        {properties && (
                            Object.keys(properties).map((key, index) => (
                                <PropItem 
                                property={properties[key]} 
                                key={index}
                                token={key}  />
                                
                            ))
                        )}
                        {!properties && (
                            <tr>
                            <td>No se encontraron resultados</td>
                            </tr>
                        )}
                        </tbody>
                        </Table>
                        {properties && (
                            Object.keys(properties).map((key, index) => (
                                <div key={index}>
                                    <ContextMenu renderTag="div" id={key}>
                                        <MenuItem data={{foo: properties[key]}} onClick={this.handleClick}>
                                        <i className="fa fa-eye" /> Ver
                                        </MenuItem>
                                        <MenuItem data={{foo: 'bar', }} onClick={this.handleClick}>
                                        <i className="fa fa-pencil" /> Editar
                                        </MenuItem>
                                        <MenuItem divider />
                                        <MenuItem data={{foo: 'bar'}} onClick={this.handleClick}>
                                        <i className="fa fa-trash" /> Borrar
                                        </MenuItem>
                                    </ContextMenu>    
                                </div>
                            ))
                        )}
                        <Pagination>
                        <PaginationItem>
                            <PaginationLink previous tag="button"></PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink next tag="button"></PaginationLink>
                        </PaginationItem>
                        </Pagination>
                    </CardBody>
                    </Card>
                </Col>
                </Row>
            </div>
            );
        }
    }
    
export default withFirebase(PropertiesIndex);