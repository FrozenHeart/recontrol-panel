import React from 'react';
import { withFirebase } from '../../components/Firebase';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import {
    Card,
    CardBody,
    CardHeader,
    CardFooter,
    Col,
    Row,
    Input,
    Label,
    Form,
    FormGroup,
    Button,

  } from 'reactstrap';

class CreateOwners extends React.Component {
    constructor (props) {
        super(props);
        
        this.state = {
            properties: [], 
            formControls: {
                name: {
                  value: ''
                },
                lastName: {
                  value: ''
                },
                createdAt: {
                  value: ''
                },
                notes: {
                    value: ''
                },
                tags: {
                    value: ''
                },
                phone: {
                    value: ''
                },
                email: {
                    value: ''
                },
                phoneHouse: {
                    value: ''
                },
                otherEmail: {
                    value: ''
                },
                phoneMovil: {
                    value: ''
                },
                fax: {
                    value: ''
                },
                address: {
                    value: ''
                },
                city: {
                    value: ''
                },
                postalCode: {
                    value: ''
                },
                cAutonoma: {
                    value: ''
                },
                province: {
                    value: ''
                },
                country: {
                    value: ''
                },
                docType: {
                    value: ''
                },
                expended: {
                    value: ''
                },
                expendedAt: {
                    value: ''
                },
                docNumber: {
                    value: ''
                },
                propertyId: {
                    value: ''
                },
            }
        }
      }
  
      componentDidMount() {
          this.loadProperties();
      }
  
      loadProperties = () => {
          this.props.firebase.db.ref().child('properties').on('value', snapshot => {
              const PropertiesList = snapshot.val();
              this.setState({
                properties: PropertiesList
              })
          });
      }
      handleInputChange = event => {
      
        const name = event.target.name;
        const value = event.target.value;
      
        this.setState({
            formControls: {
                ...this.state.formControls,
                [name]: {
                ...this.state.formControls[name],
                value
              }
            }
          });
      
    }
    static contextTypes = {
        router: PropTypes.object
    }
    onCreateOwner = (event) => {
        console.log(event);
        const values = this.state.formControls;
        this.props.firebase.db.ref().child('owners').push({
            name: values.name.value,
            lastName: values.lastName.value,
            createdAt: values.createdAt.value,
            notes: values.notes.value,
            tags: values.tags.value,
            phone: values.phone.value,
            email: values.email.value,
            phoneHouse: values.phoneHouse.value,
            otherEmail: values.otherEmail.value,
            phoneMovil: values.phoneMovil.value,
            fax: values.fax.value,
            address: values.address.value,
            city: values.city.value,
            postalCode: values.postalCode.value,
            cAutonoma: values.cAutonoma.value,
            province: values.province.value,
            country: values.country.value,
            docType: values.docType.value,
            expended: values.expended.value,
            expendedAt: values.expendedAt.value,
            docNumber: values.docNumber.value,
            propertyId: values.propertyId.value,
        })
        .then(res => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
              });
              
              Toast.fire({
                type: 'success',
                title: 'Se registró un nuevo propietario'
              })
            this.context.router.history.push('/owners');  
        });
        event.preventDefault();
    };

    render() {
        const {properties} = this.state;
        return(
            <Row>
            <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <strong>Nuevo Propietario</strong> El simbolo <strong>(*)</strong> se refiere a un campo requerido
              </CardHeader>
              <CardBody>
                <Form onSubmit={event =>
                this.onCreateOwner(event)
              } className="form-horizontal">
                  <strong>Datos propietario</strong>
                  <hr />
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="name">Nombres <strong>(*)</strong></Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="name" name="name" onChange={this.handleInputChange} />
                                
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                    <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="lastName">Apellidos <strong>(*)</strong></Label>
                        </Col>
                        <Col xs="9" md="6">
                            <Input type="text" id="lastName" name="lastName" onChange={this.handleInputChange} />
                        </Col>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="createdAt">Fecha de Alta <strong>(*)</strong></Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="date" id="createdAt" name="createdAt" placeholder="date" onChange={this.handleInputChange} required />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                    <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="createdAt">Fecha de Nacimiento</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="date" id="createdAt" name="createdAt" onChange={this.handleInputChange} placeholder="date" />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="12">
                            <Label htmlFor="notes">Observaciones</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Input type="textarea" name="notes" id="notes" onChange={this.handleInputChange} rows="3" />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="12">
                            <Label htmlFor="tags">Tags</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Input type="textarea" name="tags" id="tags" onChange={this.handleInputChange} rows="3" />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <hr />
                  <strong>Contacto</strong>
                  <hr />
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="phone">Telefono </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="phone" name="phone" onChange={this.handleInputChange} />
                                
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                    <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="email">Email</Label>
                        </Col>
                        <Col xs="9" md="6">
                            <Input type="text" id="email" name="email" onChange={this.handleInputChange}  />
                        </Col>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="phoneHouse">Telefono casa </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="phoneHouse" name="phoneHouse" onChange={this.handleInputChange}  />
                                
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                    <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="otherEmail">Email alternativo</Label>
                        </Col>
                        <Col xs="9" md="6">
                            <Input type="text" id="otherEmail" name="otherEmail" onChange={this.handleInputChange}  />
                        </Col>
                    </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="phoneMovil">Telefono móvil </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="phoneMovil" name="phoneMovil" onChange={this.handleInputChange}  />
                                
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                    <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="fax">Fax</Label>
                        </Col>
                        <Col xs="9" md="6">
                            <Input type="text" id="fax" name="fax"  onChange={this.handleInputChange} />
                        </Col>
                    </FormGroup>
                    </Col>
                  </Row>
                  <hr />
                  <strong>Domicilio</strong>
                  <hr />
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="address">Dirección </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="address" name="address" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="city">Ciudad</Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="city" name="city" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="postalCode">Código Postal </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="postalCode" name="postalCode" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="cAutonoma">C. Autónoma</Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="cAutonoma" name="cAutonoma" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="province">Provincia </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="province" name="province" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="country">País</Label>
                            </Col>
                            <Col xs="9" md="6">
                            <Input type="select" name="country" id="country" onChange={this.handleInputChange}>
                                <option value="0">España</option>
                            </Input>
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <hr />
                  <strong>Documentación</strong>
                  <hr />
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="docType">Tipo documento </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="select" name="docType" id="docType" onChange={this.handleInputChange}>
                                    <option value="0">- Seleccione -</option>
                                </Input>
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="expended">Expendido lugar</Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="expended" name="expended" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="docNumber">N° documento </Label>
                            </Col>
                            <Col xs="9" md="6">
                                <Input type="text" id="docNumber" name="docNumber" onChange={this.handleInputChange} />
                                
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col md="6">
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="expendedAt">Expendido fecha</Label>
                            </Col>
                            <Col xs="9" md="6">
                            <Input type="date" id="expendedAt" name="expendedAt" placeholder="date" onChange={this.handleInputChange} />
                            </Col>
                        </FormGroup>
                    </Col>
                  </Row>


                  <hr />
                  
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="propertyId">Propiedad</Label>
                    </Col>
                    <Col xs="9" md="6">
                      <Input type="select" name="propertyId" id="select" onChange={this.handleInputChange} >
                        <option value="0">- Ninguna -</option>
                        {properties && (
                          Object.keys(properties).map((key, index) => (
                            <option value={key} key={index}>{properties[key].name}</option>  
                          ))
                        )}
                      </Input>
                    </Col>
                    <Col md="1">
                        <Button block outline color="danger" style={{padding: "0rem 0rem" }} size="sm">Nuevo</Button>
                    </Col>
                  </FormGroup>
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                </Form>
              </CardBody>
              <CardFooter>
                
              </CardFooter>
            </Card>

            </Col>
        </Row>
        
        )
    }
}

export default withFirebase(CreateOwners)