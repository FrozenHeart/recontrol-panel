import React from 'react';
import { withFirebase } from '../../components/Firebase';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';

class ViewOwner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ownerId: this.props.match.params.id,
            owner: []
        }
    }
    static contextTypes = {
        router: PropTypes.object
    }
    componentDidMount () {
        this.props.firebase.db.ref('owners').child(this.props.match.params.id).on('value', snapshot => {
            if(!snapshot.exists())
            {
                const values = snapshot.val(); 
                this.setState({
                    owner: values
                });
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                  });
                  
                  Toast.fire({
                    type: 'error',
                    title: 'Este propietario ya no existe'
                  })
                this.context.router.history.push('/owners');
            }
        });
    }
    render () {
        return (
            <div>
            Existe {this.state.ownerId}</div>
        )
    }
}
export default withFirebase(ViewOwner)