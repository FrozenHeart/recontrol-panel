import React from 'react';
import { withFirebase } from '../../components/Firebase';
import OwnerItem from '../../components/Owners/OwnerItem';
import { ContextMenu, MenuItem } from "react-contextmenu";
import PropTypes from 'prop-types'
import {
    Pagination,
    PaginationItem,
    PaginationLink,
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
    Button,
  } from 'reactstrap';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

class OwnersIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            owners: []
        }
    }
    componentDidMount() {
        this.loadOwners();
    }
    loadOwners = () => {
        this.props.firebase.db.ref().child('owners').orderByChild('createdAt').on('value', snapshot => {
            const OwnersList = snapshot.val();
            this.setState({
                owners: OwnersList
            })
        });
    }
    static contextTypes = {
        router: PropTypes.object
    }
    redirectToTarget = () => {
        this.context.router.history.push('/owners/create')
    }
    redirectToOwner = (id) => {
        this.context.router.history.push('/owners/'+id);
    }
    removeOwner = (uid) => {
        this.props.firebase.db.ref('owners').child(uid).remove();
    }
    handleClick(e, data) {
        if(data.action === 'view')
        {
            data.page.redirectToOwner(data.uid);
        }
        if(data.action === 'delete'){
            const MySwal = withReactContent(Swal)

            MySwal.fire({
                title: '¿Estás seguro?',
                text: "Si eliminas a este propietario no podrás recuperar su información",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Borrar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if(result.value)
                {
                    data.page.removeOwner(data.uid);
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                      });
                      
                      Toast.fire({
                        type: 'success',
                        title: 'El propietario fue eliminado de la base de datos.'
                      })
                    
                }
                
            })
        }
    }
    render() {
        const { owners } = this.state;

        return (
        <div className="animated fadeIn">
            <Row>
            <Col xs="12" sm="12" lg="12">
                <Card>
                <CardHeader>
                    Propietarios
                </CardHeader>
                <CardBody>
                    <Card>
                        <CardBody>
                            <Row>
                                <Col xs="12" sm="12" md="9" lg="9" style={{ textAlign: "center" }}>
                                    <p>A continuación se encuentra la tabla con todos los propietarios que se registraron, puede presionar el click derecho sobre una propiedad para realizar acciones con uno de ellos.</p>
                                </Col>
                                <Col xs="12" sm="12" md="3" lg="3">
                                <Button block color="primary" onClick={this.redirectToTarget}>Registrar Propietario</Button>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                    <Table responsive>
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Propiedad</th>
                        <th>Telefono</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    {owners && (
                        Object.keys(owners).map((key, index) => (
                            <OwnerItem 
                            owner={owners[key]} 
                            key={index}
                            token={key}  />
                            
                        ))
                    )}
                    {!owners && (
                        <tr>
                        <td>No se encontraron resultados</td>
                        </tr>
                    )}
                    </tbody>
                    </Table>
                    {owners && (
                        Object.keys(owners).map((key, index) => (
                            <div key={index}>
                                <ContextMenu renderTag="div" id={key}>
                                    <MenuItem data={{action: 'view', uid: key, page: this}} onClick={this.handleClick}>
                                    <i className="fa fa-eye" /> Ver
                                    </MenuItem>
                                    <MenuItem data={{action: 'edit', uid: key, page: this}} onClick={this.handleClick}>
                                    <i className="fa fa-pencil" /> Editar
                                    </MenuItem>
                                    <MenuItem divider />
                                    <MenuItem data={{action: 'delete', uid: key, page: this}} onClick={this.handleClick}>
                                    <i className="fa fa-trash" /> Eliminar
                                    </MenuItem>
                                </ContextMenu>    
                            </div>
                        ))
                    )}
                    <Pagination>
                    <PaginationItem>
                        <PaginationLink previous tag="button"></PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink next tag="button"></PaginationLink>
                    </PaginationItem>
                    </Pagination>
                </CardBody>
                </Card>
            </Col>
            </Row>
        </div>
        );
    }
}

export default withFirebase(OwnersIndex)