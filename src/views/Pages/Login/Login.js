import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { withFirebase } from '../../../components/Firebase';
import { compose } from 'recompose';


const Login = () => (
  <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                  <LoginForm />
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>¿No eres cliente?</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Contacto</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class LoginFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push('/');
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password === '' || email === '';
    return (
      <Form onSubmit={this.onSubmit}>
        <h1>Login</h1>
          <p className="text-muted">Ingresa tus datos de acceso</p>
          <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-user"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input type="email" name="email" placeholder="Correo Electrónico" autoComplete="email" value={email} onChange={this.onChange} />
        </InputGroup>
        <InputGroup className="mb-4">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-lock"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input type="password" name="password" value={password} onChange={this.onChange} placeholder="Contraseña" autoComplete="current-password" />
        </InputGroup>
        {error && <Row><p>{error.message}</p></Row>}
        <Row>
          <Col xs="6">
            <Button color="primary" disabled={isInvalid} className="px-4">Login</Button>
          </Col>
          <Col xs="6" className="text-right">
            <Button color="link" className="px-0">¿Olvidaste tu contraseña?</Button>
          </Col>
        </Row>
      </Form>
                  
    );
  }
}


const LoginForm = compose(
  withRouter,
  withFirebase,
)(LoginFormBase);

export default Login;

export { LoginForm };
