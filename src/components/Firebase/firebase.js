import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyC5XAIyAAe1KUIUBi_8q9Y0nrW6SOqWewQ",
  authDomain: "recontrol-dev.firebaseapp.com",
  databaseURL: "https://recontrol-dev.firebaseio.com",
  projectId: "recontrol-dev",
  storageBucket: "recontrol-dev.appspot.com",
  messagingSenderId: "1081964858265"
};

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
    this.db = app.database();
  }

  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  createUserInDB(uid, email)
  {
    this.db.ref().child('users').orderByChild("email").equalTo(email).once("value",snapshot => {
      if (!snapshot.exists()){
        this.db.ref('users').child(uid).set({
          email,
          name: 'Admin',
          admin: true,
          avatar: '../../assets/img/avatars/6.jpg',
        });
      }
    });
  }

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password).then(res => {
      this.createUserInDB(
        res.user.uid,
        res.user.email
      );
    });

  doSignOut = () => this.auth.signOut();

  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

  // *** User API ***

  user = uid => this.db.ref(`users/${uid}`);

  users = () => this.db.ref('users');
}

export default Firebase;
