import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withAuth } from './context';
import { withFirebase } from '../Firebase';
const withAuthorization = condition => Component => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      this.listener = this.props.firebase.auth.onAuthStateChanged(
        authUser => {
          if (!condition(authUser)) {
            this.props.history.push('/login');
          }
        },
      );
    }

    componentWillUnmount() {
      this.listener();
    }

    render() {
      if(condition(this.props.authUser))
      {
        return (
          <Component {...this.props} />
        );
      }
      else {
        return(
          <p>No logueado {this.props.authUser}</p>
        )
        
      }
      
    }
  }

  return compose(withFirebase, withRouter, withAuth)(WithAuthorization);
};

export default withAuthorization;
