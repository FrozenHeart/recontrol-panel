import React from 'react';

import AuthUserContext from './context';
import { withFirebase } from '../Firebase';

const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        authUser: null,
        useruid: null,
        userInfo: null
      };
    }
      
    componentDidMount() {
      this.listener = this.props.firebase.auth.onAuthStateChanged(
        authUser => {
          if(authUser)
          {
            this.setState({ useruid: authUser.uid, authUser });
            this.onListenForChanges()
          }
          else
          {
            this.props.history.push('/login');
          }
        },
      );

      
    }

    onListenForChanges = () => {
      this.props.firebase.db.ref('users').child(this.state.useruid)
        .on('value', snapshot => {
          const userObject = snapshot.val();
          if (userObject) {
            this.setState({
              userInfo: userObject
            });
          } else {
            this.setState({ userInfo: 'Nulo'});
          }
        });
    };

    componentWillUnmount() {
      this.listener();
    }

    render() {
      return (
        <AuthUserContext.Provider value={this.state.authUser}>
          <Component token={this.state.useruid} userInfo={this.state.userInfo} authUser={this.state.authUser} {...this.props} />
        </AuthUserContext.Provider>
      );
    }
  }

  return withFirebase(WithAuthentication);
};

export default withAuthentication;
