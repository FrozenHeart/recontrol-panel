import React from 'react';
import { withFirebase } from '../Firebase';
import { ContextMenuTrigger } from "react-contextmenu";
import {
    Badge
  } from 'reactstrap';

    class PropItem extends React.Component {
        constructor (props) {
            super(props);
            this.state = {
                property: null,
                owner: null
            };
        }
        
        componentDidMount () {
            this.setState({
                property: this.props.property
            });
            this.props.firebase.db.ref().child('owners').orderByChild("propertyId").equalTo(this.props.token).once("value",snapshot => {
                
                if(snapshot.exists())
                {
                    var ownerInfo = snapshot.val();
                    Object.keys(ownerInfo).map((key, index) =>(
                        ownerInfo = ownerInfo[key]
                     ));
                     this.setState({
                        owner: ownerInfo
                     });
                    
                }
            });
        }
        
        render () {
            const {owner} = this.state; 
            
            return (
                    
                    <ContextMenuTrigger renderTag="tr" id={this.props.token}>
             
                            <td>{this.props.property.name}</td>
                            {owner && (
                                <td>{owner.name}</td>
                            )  
                            }
                            {!owner && (
                                <td>Desconocido</td>
                            )  
                            }
                            <td>Algo </td>
                            <td>
                                <Badge color="warning">Bloqueada</Badge>
                            </td>
                        
                    </ContextMenuTrigger>
            )
        }
        
    }
    
export default withFirebase(PropItem)