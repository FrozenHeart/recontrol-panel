import React from 'react';
import { withFirebase } from '../Firebase';
import { ContextMenuTrigger } from "react-contextmenu";


    class OwnerItem extends React.Component {
        constructor (props) {
            super(props);
            this.state = {
                property: null,
                owner: []
            };
        }
        
        componentDidMount () {
            this.setState({
                owner: this.props.owner
            });
            if(this.props.owner.propertyId)
            {
                this.props.firebase.db.ref('properties').child(this.props.owner.propertyId).once("value",snapshot => {
                
                    if(snapshot.exists())
                    {
                        var propertyInfo = snapshot.val();
                        Object.keys(propertyInfo).map((key, index) =>(
                            propertyInfo = propertyInfo[key]
                         ));
                         this.setState({
                            property: propertyInfo
                         });
                        
                    }
                });
            }
            
        }
        
        render () {
            const {owner, property} = this.state; 
            
            return (
                    
                    <ContextMenuTrigger renderTag="tr" id={this.props.token}>
             
                            <td>{owner.name}</td>
                            {property && (
                                <td>{property.name}</td>
                            )  
                            }
                            {!property && (
                                <td>Sin asignar</td>
                            )  
                            }
                            <td>{owner.phone}</td>
                            <td>
                                {owner.email}
                            </td>
                        
                    </ContextMenuTrigger>
            )
        }
        
    }
    
export default withFirebase(OwnerItem)